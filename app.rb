require 'sinatra'
require 'active_record'
require './config/environments' #database configuration
require './models/model'


before do
  begin
    request.body.rewind
    @json_data = JSON.parse(request.body.read.to_s)
  rescue
    @json_data = {}
  end
end


get '/' do
	'Hello world!'
end

get '/test/:id' do
	Model.where(:id => params['id']).first.to_json
end

get '/tests' do
	Model.all.to_json
end

post '/test' do
  if @json_data.has_key?('name')
    newTest = Model.new(:name => @json_data['name'])
    newTest.save
    return {'id' => newTest.id}.to_json
  end
  {'id' => 0}.to_json
end


post '/test/:id' do
  test = Model.where(:id => params['id']).first
  if @json_data.has_key?('name') and not test.nil?
	test.name = @json_data['name']
	test.save
	return {'id' => test.id}.to_json
  end
  {'id' => 0}.to_json
end


post '/test/destroy/:id' do

  test = Model.where(:id => params['id']).first
  if not test.nil? then
	if test.destroy then
		return {'success' => true}.to_json
	end
  end

  {'success' => false}.to_json
end

