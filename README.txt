COMMANDS USED


cd ~/.pow
ln -s ~/dev/ruby/sinatra1


cd ~/dev/ruby/sinatra1

mkdir tmp

touch tmp/always_restart.txt


xcode-select --install


rvm install ruby-2.0.0-p451

rvm use 2.0.0-p451 --default


sudo gem install bundler

sudo bundle install


install postgres + create mydb in postgres

psql \postgres  <=== verify

\q  <===== quit

rake db:migrate


== test local ==


heroku login

heroku keys:add

heroku create

git push heroku master


heroku addons:add heroku-postgresql:mydb

heroku config | grep HEROKU_POSTGRESQL

heroku pg:promote HEROKU_POSTGRESQL_NAVY_URL   <==== app uses environment variable for db name; this sets it

heroku run rake db:migrate    <==== creates db from migration data


